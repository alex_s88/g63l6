//
//  Phone.swift
//  G63L5
//
//  Created by Alexandr Sopilnyak on 16.06.2018.
//  Copyright © 2018 Alexandr Sopilnyak. All rights reserved.
//

import UIKit

class Phone: NSObject {

    private static var phonesMade = 0
    
    let screenSize: Double
    let memorySize:  Int
    let color = "white"
    
    var freeMemory = 0
    var photosCount: Int {
        return _photosCount
    }
    var phoneName: String?
    override var description: String {
        var result = ""
        result += "phoneName = \(phoneName ?? "unknown")\n"
        result += "photosCount = \(_photosCount)\n"
        result += "freeMemody = \(freeMemory)GB\n"
        result += "total phones count  = \(Phone.phonesMade)\n"
        return result
    }
    
    private var _photosCount = 0  //private var
    
    init(screen: Double, memory: Int) {
        Phone.phonesMade += 1
        screenSize = screen
        memorySize = memory
        freeMemory = memory - 1
    }
    
    func makePhoto() {
        _photosCount += 1
    }
    
    
    
}


