//
//  ViewController.swift
//  G63L6
//
//  Created by Alexandr Sopilnyak on 17.06.2018.
//  Copyright © 2018 Alexandr Sopilnyak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    enum JoysticButton: Int {
        case xMinus
        case xPlus
        case yPlus
        case yMinus
    }
    
    @IBOutlet weak var centerLabel: UILabel!
    var phone: Phone!
    var xPosition = 0
    var yPosition = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        phone = Phone(screen: 5.5, memory: 32)
    }
    @IBAction func buttonPressed() {
        let texpToDisplay = "Button pressed. \(arc4random()%100)"
        print("Button pressed.")
        centerLabel.text = texpToDisplay
        
    }
    
    @IBAction func shootPressed() {
        phone.makePhoto()
    }
    
    @IBAction func descriptionButtonPressed() {
        centerLabel.text = phone.description
    }
    
    @IBAction func changePositionButtonPressed(_ sender: UIButton) {
//        if sender.tag == 1 {
//            xPosition += 1
//        }
//        else {
//            xPosition -= 1
//        }
//        sender.tag == 1 ? (xPosition += 1) : (xPosition -= 1)
//        xPosition += (sender.tag == 1) ? 1 : -1
        
        if let joysticButton = JoysticButton(rawValue: sender.tag) {
            switch joysticButton {
            case .xMinus:
                xPosition -= 1
            case .xPlus:
                xPosition += 1
            case .yMinus:
                yPosition -= 1
            case .yPlus:
                yPosition += 1
            }
        }
        else {
            print("Unknown tag. \(sender.tag)")
        }
        
//        switch sender.tag {
//        case 1:
//            xPosition += 1
//        case 0:
//            xPosition -= 1
//        default:
//            print("Unknown tag. \(sender.tag)")
//        }
        
        centerLabel.text = "x \(xPosition.description) y \(yPosition.description)"
        print(xPosition)
    }
    
    
    
}

